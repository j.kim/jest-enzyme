import React from 'react';
import Link from './templates/Link';
import Title from './templates/Title';
import Input from './templates/Input';
import Button from './templates/Button';
import Checkbox from './templates/Checkbox';
import Dropdown from './templates/Dropdown';
import Textarea from './templates/Textarea';

export class App extends React.Component {
  constructor() {
    super();
    this.state = {
      linkState: { state: '', page: '' },
      inputState: { inputValue: '' },
      buttonState: { text: '' },
      checkobxState: { isChecked: false },
      dropdownState: getDropdownState(""),
      textareaState: { textareaValue: '' },
    }
    this.handleOnMouseEnter = this.handleOnMouseEnter.bind(this);
    this.handleOnMouseLeave = this.handleOnMouseLeave.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleOnChangeCheckbox = this.handleOnChangeCheckbox.bind(this);
    this.handleOnChangeDropdown = this.handleOnChangeDropdown.bind(this);
    this.handleOnChangeTextarea = this.handleOnChangeTextarea.bind(this);
  }

  handleOnMouseEnter() {
    this.setState({
      linkState: {
        state: "enter",
        page: this.state.linkState.page
      },
    });
  }

  handleOnMouseLeave() {
    this.setState({
      linkState: {
        state: "leave",
        page: this.state.linkState.page
      },
    });
  }

  handleChange(value) {
    this.setState({
      inputState: {
        inputValue: value,
      },
    });
  }

  handleClick() {
    this.setState({
      buttonState: {
        text: this.state.inputState.inputValue,
      }
    });
  }

  handleOnChangeCheckbox(value) {
    this.setState({
      checkobxState: {
        isChecked: value,
      }
    });
  }

  handleOnChangeDropdown(value) {
    this.setState({
      dropdownState: getDropdownState(value),
    });
  }

  handleOnChangeTextarea(value) {
    this.setState({
      textareaState: {
        textAreaValue: value,
      },
    });
  }

  render() {
    return (
      <div>
        <Link handleOnMouseEnter={this.handleOnMouseEnter} handleOnMouseLeave={this.handleOnMouseLeave} page={this.state.linkState.page} />
        <Title text={this.state.buttonState.text} />
        <Input handleChange={this.handleChange} value={this.state.inputState.inputValue} />
        <Button handleClick={this.handleClick} />
        <Checkbox handleOnChangeCheckbox={this.handleOnChangeCheckbox} />
        <Dropdown handleOnChangeDropdown={this.handleOnChangeDropdown} itemList={this.state.dropdownState.itemList} />
        <Textarea handleChange={this.handleChange} value={this.state.textareaState.textareaValue} />
      </div>
    );
  }
}

function getDropdownState(item) {
  return {
    selectedItem: item,
    itemList: [1, 2, 3, 4, 5],
  };
}