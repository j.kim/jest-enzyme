import React from 'react';

export default function Link(props) {
    function handleOnMouseEnter() {
        props.handleOnMouseEnter();
    }

    function handleOnMouseLeave() {
        props.handleOnMouseLeave();
    }

    return (
        <a
            href={props.page}
            onMouseOver={handleOnMouseEnter}
            onMouseLeave={handleOnMouseLeave}
        >
        </a>
    );
}

