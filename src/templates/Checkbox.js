import React from 'react';

export default function Checkbox(props) {
	function handleOnChangeCheckbox(event) {
		props.handleOnChangeCheckbox(event.target.checked);
	}

	return (
		<input
			type="checkbox"
			checked={props.isChecked}
			onChange={handleOnChangeCheckbox}
		/>
	);

}