import React from 'react';

export default function Dropdown(props) {

	const items = props.itemList;
	const itemListElem = items.map((item) =>
		<option key={item.toString()}>
			{item}
		</option>
	);

	function handleOnChangeDropdown(event) {
		props.handleOnChangeDropdown(event.target.value);
	}

	return (
		<select id="dropdown" onChange={handleOnChangeDropdown}>
			{itemListElem}
		</select>
	);

}


