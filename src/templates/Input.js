import React from 'react';

export default function Input(props) {
	function handleChange(event) {
		props.handleChange(event.target.value);
	}

	return (
		<input onChange={handleChange} value={props.value} />
	);
}