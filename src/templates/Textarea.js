import React from 'react';

export default function Dropdown(props) {
    function handleChange(event) {
        props.handleChange(event.target.value);
    }

    return (
        <textarea onChange={handleChange} value={props.value} />
    );
}


