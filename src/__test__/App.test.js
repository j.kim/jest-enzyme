import React from 'react';
import ReactDOM from 'react-dom';
import { App } from '../App';
import Link from '../templates/Link';
import Title from '../templates/Title';
import Input from '../templates/Input';
import Button from '../templates/Button';
import Checkbox from '../templates/Checkbox';
import Dropdown from '../templates/Dropdown';
import Textarea from '../templates/Textarea';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

/**
 * Appコンポーネントのテスト
 */
describe('<App />', () => {
  it('子コンポーネントが存在すること', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find(Title).length).toBe(1);
    expect(wrapper.find(Input).length).toBe(1);
    expect(wrapper.find(Button).length).toBe(1);
  });

  it('handleChangeを呼び出すと、setStateが呼び出されること', () => {
    const wrapper = shallow(<App />);
    const setStateSpy = jest.spyOn(App.prototype, 'setState');
    wrapper.instance().handleChange('value');
    expect(setStateSpy).toHaveBeenCalledWith({
      inputState: {
        inputValue: 'value'
      }
    });
  });

  it('handleClickを呼び出すと、setStateが呼び出されること', () => {
    const wrapper = shallow(<App />);
    const setStateSpy = jest.spyOn(App.prototype, 'setState');
    wrapper.setState({
      inputState: {
        inputValue: 'value'
      }
    });
    wrapper.instance().handleClick();
    expect(setStateSpy).toHaveBeenCalledWith({
      buttonState: {
        text: 'value'
      }
    });
  });

  it('handleOnChangeCheckboxを呼び出すと、setStateが呼び出されること', () => {
    const wrapper = shallow(<App />);
    const setStateSpy = jest.spyOn(App.prototype, 'setState');
    wrapper.setState({
      isChecked: false
    });
    wrapper.instance().handleOnChangeCheckbox(true);
    expect(setStateSpy).toHaveBeenCalledWith({
      checkobxState: {
        isChecked: true
      }
    });
  });

  it('handleOnChangeDropdownを呼び出すと、setStateが呼び出されること', () => {
    const wrapper = shallow(<App />);
    const setStateSpy = jest.spyOn(App.prototype, 'setState');
    wrapper.instance().handleOnChangeDropdown('value');
    expect(setStateSpy).toHaveBeenCalledWith({
      dropdownState: {
        itemList: [1, 2, 3, 4, 5],
        selectedItem: "value"
      },
    });
  });

  it('handleOnChangeTextareaを呼び出すと、setStateが呼び出されること', () => {
    const wrapper = shallow(<App />);
    const setStateSpy = jest.spyOn(App.prototype, 'setState');
    wrapper.instance().handleOnChangeTextarea('value');
    expect(setStateSpy).toHaveBeenCalledWith({
      textareaState: {
        textAreaValue: 'value'
      },
    });
  });

  test('<App />のスナップショット', () => {
    const tree = renderer
      .create(<App />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

/**
 * Linkコンポーネントのテスト
 */
describe('<Link />', () => {
  it('a要素が存在すること', () => {
    const wrapper = shallow(<Link page={'https://google.com'} />);
    expect(wrapper.find('a').length).toBe(1);
  });

  it('mouseoverイベント発火時にコールバック関数が呼び出されること', () => {
    const func = jest.fn();
    const wrapper = shallow(<Link handleOnMouseEnter={func} />);

    wrapper.find('a').simulate('mouseover');
    expect(func).toHaveBeenCalled();
  });

  it('mouseleaveイベント発火時にコールバック関数が呼び出されること', () => {
    const func = jest.fn();
    const wrapper = shallow(<Link handleOnMouseLeave={func} />);

    wrapper.find('a').simulate('mouseleave');
    expect(func).toHaveBeenCalled();
  });

  test('<Link />のスナップショット', () => {
    const func = jest.fn();
    const page = 'https://google.com';
    const tree = renderer
      .create(<Input handleOnMouseEnter={func} page={page} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

/**
 * Titleコンポーネントのテスト
 */
describe('<Title />', () => {
  it('h1要素が存在すること', () => {
    const wrapper = shallow(<Title text={'Hello'} />);
    expect(wrapper.find('h1').length).toBe(1);
  });

  it('受け取ったpropsの値を表示すること', () => {
    const wrapper = shallow(<Title text={'Hello'} />);
    expect(wrapper.text()).toBe('Hello');
    wrapper.setProps({ text: 'World' });
    expect(wrapper.text()).toBe('World');
  });

  test('<Title />のスナップショット', () => {
    const tree = renderer
      .create(<Title text={'Hello'} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

/**
 * Inputコンポーネントのテスト
 */
describe('<Input />', () => {
  it('input要素が存在すること', () => {
    const wrapper = shallow(<Input />);
    expect(wrapper.find('input').length).toBe(1);
  });

  it('changeイベント発火時にコールバック関数が呼び出されること', () => {
    const func = jest.fn();
    const wrapper = shallow(<Input handleChange={func} />);

    const event = { target: { value: 'value' } };
    wrapper.find('input').simulate('change', event);
    expect(func).toHaveBeenCalledWith('value');
  });

  test('<Input />のスナップショット', () => {
    const func = jest.fn();
    const value = 'value';
    const tree = renderer
      .create(<Input onChange={func} value={value} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

/**
 * Buttonコンポーネントのテスト
 */
describe('<Button />', () => {
  it('button要素が存在すること', () => {
    const wrapper = shallow(<Button />);
    expect(wrapper.find('button').length).toBe(1);
  });

  it('clickイベント発火時にコールバック関数が呼び出されること', () => {
    const func = jest.fn();
    const wrapper = shallow(<Button handleClick={func} />);
    wrapper.find('button').simulate('click');
    expect(func).toHaveBeenCalled();
  });

  test('<Button />のスナップショット', () => {
    const func = jest.fn();
    const tree = renderer
      .create(<Button handleClick={func} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

/**
 * Checkboxコンポーネントのテスト
 */
describe('<Checkbox />', () => {
  it('checkbox要素が存在すること', () => {
    const wrapper = shallow(<Checkbox />);
    expect(wrapper.find('input').length).toBe(1);
  });

  it('チェック変更イベント発火時にコールバック関数が呼び出されること', () => {
    const func = jest.fn();
    const wrapper = shallow(<Checkbox handleOnChangeCheckbox={func} />);

    const event = { target: { checked: true } };
    wrapper.find('input').simulate('change', event);
    expect(func).toHaveBeenCalled();
  });

  test('<Checkbox />のスナップショット', () => {
    const func = jest.fn();
    const tree = renderer
      .create(<Checkbox handleOnChangeCheckbox={func} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

/**
 * Dropdownコンポーネントのテスト
 */
describe('<Dropdown />', () => {

  const itemList = [1, 2, 3, 4, 5];

  it('Dropdown要素が存在すること', () => {
    const wrapper = shallow(<Dropdown itemList={itemList} />);
    expect(wrapper.find('select').length).toBe(1);
  });

  it('選択変更イベント発火時にコールバック関数が呼び出されること', () => {
    const func = jest.fn();
    const wrapper = shallow(<Dropdown handleOnChangeDropdown={func} itemList={itemList} />);

    const event = { target: { value: "1" } };
    wrapper.find('select').simulate('change', event);

    expect(func).toHaveBeenCalled();
  });

  test('<Dropdown />のスナップショット', () => {
    const func = jest.fn();
    const tree = renderer
      .create(<Dropdown handleOnChangeDropdown={func} itemList={itemList} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

/**
 * TextAreaコンポーネントのテスト
 */
describe('<Textarea />', () => {
  it('Textarea要素が存在すること', () => {
    const wrapper = shallow(<Textarea />);
    expect(wrapper.find('textarea').length).toBe(1);
  });

  it('Textareaにイベント発火時にコールバック関数が呼び出されること', () => {
    const func = jest.fn();
    const wrapper = shallow(<Textarea handleChange={func} />);

    const event = { target: { value: 'value' } };
    wrapper.find('textarea').simulate('change', event);
    expect(func).toHaveBeenCalledWith('value');
  });

  test('<Textarea />のスナップショット', () => {
    const func = jest.fn();
    const value = 'value';
    const tree = renderer
      .create(<Textarea onChange={func} value={value} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});